Scrap bookmark server
=====================

A simple, single file, bookmark server designed to share sites between browsers
and reduce your population of open tabs.

Features
--------

* Bookmarklet-based addition of new sites
* Dynamic layout for desktop and mobile
* Keyboard navigation - find as you type, cursor control
* In-page editable bookmark titles
* Link click counting (data unused, so far)
* Site-based bookmark customisation (via editing the script)
* JSON bookmark file - can be version controlled in Git, if needed

Installation
------------

1. Install some perl modules. On Ubuntu, you need something like:

  `sudo apt-get install libjson-perl libhtml-tokeparser-simple-perl liburi-encode-perl libwww-perl libfile-slurp-perl libnet-server-perl`

2. Start the server with:
  `./scrap.pl`

3. Visit `localhost:2222/addml` and add the bookmarklet to your browser.

4. Start adding bookmarks using the bookmarklet.

5. Visit `localhost:2222` to see the results.

Controls
--------

Keyboard controls are provided, to prevent keyboard-to-mouse switching whilst
using find as you type.

* The input search box should be always activated by the page
  * ie. Just start typing to do a keyword search
* Use up/down keys to select the bookmark you want
* Press enter to visit it
* Press escape to clear the search box
