#!/usr/bin/perl

use JSON;
use HTML::TokeParser;
use LWP::Simple;
use Date::Parse;
use File::Slurp;
use Digest::MD5 qw/md5_hex/;
use URI::Encode qw/uri_decode/;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

my $dbfile = 'scrap.json';
my $port = 2222;

my $ctjs = 'application/json';
my %api = (
  'GET|' => { ct => 'text/html', fn => \&get_index},
  'GET|addml' => { ct => 'text/html', fn => \&add_marklet},
  'GET|list' => { ct => $ctjs, fn => \&list_bookmarks},
  'POST|add' => { ct => $ctjs, fn => \&add_bookmark},
  'POST|edit' => { ct => $ctjs, fn => \&edit_bookmark},
  'POST|visit' => { ct => $ctjs, fn => \&log_visit},
  'POST|delete' => { ct => $ctjs, fn => \&delete_bookmark},
  'OPTIONS|' => { ct => 'text/plain', fn => sub { print "okok\n"; }},
);

use base qw/Net::Server::HTTP/;
__PACKAGE__->run(port => [$port]);

sub process_http_request {
  my $self = shift;
  my $method = $ENV{REQUEST_METHOD};
  my(undef, $cmd, @args) = split(/\//, $ENV{REQUEST_URI});

  $method =~ s/\|//g;
  $cmd =~ s/\|//g;
  my $ep = "$method|$cmd";

  print cors_header();
  if (defined($api{$ep})) {
    print "Content-type: $api{$ep}->{ct}\n\n";
    print $api{$ep}->{fn}->($args[0]);
  } else {
    $self->send_status(404, 'Fot nound');
  }
}

sub cors_header {
  return <<HD;
Access-Control-Allow-Origin: *
Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE
Access-Control-Allow-Headers: Content-Type
Access-Control-Max-Age: 86400
HD
}

sub bookmark_exists {
  return defined($db->{hashed(shift())});
}

sub failed {
  my %r = (result => failed, reason => shift() );
  return encode_json(\%r);
}

sub log_visit {
  my $data = get_json();
  return failed('no url') unless (defined($data->{url}));
  my $db = restore_db();
  my $hurl = hashed($data->{url});
  my $v = ++($db->{$hurl}->{visit});
  $db->{$hurl}->{visited} = time();
  store_db($db);
  my %result = (result => 'success', visit => $v);
  return encode_json(\%result);
}

sub add_marklet {
  my $url = shift;
  if (defined($url)) {
    $url = uri_decode($url);
    my %data = (url => $url);
    add_bookmark('marklet', \%data);
    return <<DONE
<html><body onload="setTimeout(function(){history.back()}, 2000)">
Added $url. Returning...
</body></html>
DONE
  } else {
    my $host = qx/hostname/;
    chomp($host);
    return <<JS
<html>
<body>
Drag this to your bookmarks: <a href="javascript:window.location='http://$host:$port/addml/'+encodeURIComponent(window.location)">Scrap</a><br/>
Or copy and paste: javascript:window.location='http://$host:$port/addml/'+encodeURIComponent(window.location)<br/><br/>
Then, click the button when visiting a page that you want to bookmark.
</body></html>
JS
  }
}

sub add_bookmark {
  my $mode = shift;
  my $data = shift;
  unless ($mode eq 'marklet') {
    $data = get_json();
  }

  return failed('no url') unless (defined($data->{url}));
  my $pd = eat_page($data->{url});

  my $db = restore_db();

  return failed('url exists') if (defined($db->{hashed($pd->{url})}));
  my %mark;
  $mark{url} = $pd->{url};
  $mark{title} = $pd->{title};
  $mark{added} = time();
  $mark{expires} = $pd->{expires} if (defined($pd->{expires}));
  $mark{visit} = 0;
  $mark{domain} = $pd->{domain};

  my $hsh = hashed($pd->{url});
  $db->{$hsh} = \%mark;

  store_db($db);

  my %result = (result => 'success', hash => $hsh, data => \%mark);
  return encode_json(\%result);
}

sub edit_bookmark {
  my $data = get_json();
  return failed('no hash') unless (defined($data->{hash}));
  return failed('no title') unless (defined($data->{title}));
  my $db = restore_db();
  return failed('unknown hash') unless (defined($db->{$data->{hash}}));
  $data->{title} =~ s/\<br\>$//;  # Remove br added by browser.
  if ($data->{title} eq '') {
    delete($db->{$data->{hash}});
  } else {
    $db->{$data->{hash}}->{title} = $data->{title};
  }
  store_db($db);
  my %result = (result => 'success');
  return encode_json(\%result);
}

sub delete_bookmark {
  my $data = get_json();
  return failed('no hash') unless (defined($data->{hash}));
  my $db = restore_db();
  return failed('unknown hash') unless (defined($db->{$data->{hash}}));
  delete($db->{$data->{hash}});
  store_db($db);
  my %result = (result => 'success');
  return encode_json(\%result);
}

sub egg_spires {
  my $r = shift;
  return 4000000000 unless (defined($r->{expires}));
  return $r->{expires};
}

sub get_domain_colour {
  my $d = shift;
  $d = hex(substr(md5_hex($d), 0, 6));
  $d &= 0x3f3f3f;
  return sprintf('#%06x', $d + 0xc0c0c0);
}

sub get_result_by_domain {
  my $needle = lc(shift());
  my @result;
  my %cols;
  my $now = time();
  my $oneday = 60 * 60 * 24;
  my $db = restore_db();
  foreach my $k (sort {
      my $xa = $db->{$a};
      my $xb = $db->{$b};
      ($xa->{domain} cmp $xb->{domain}) ||
      (egg_spires($xa) cmp egg_spires($xb)) ||
      ($xa->{title} cmp $xb->{title});
    } keys(%$db)) {
    my $bm = $db->{$k};
    my $dom = $bm->{domain};
    my $text = lc("$dom $bm->{title}");
    next if (($needle ne '') and (index($text, $needle) < 0));
    ($cols{$dom} = get_domain_colour($dom)) unless (defined($cols{$dom}));
    if ((egg_spires($bm) + $oneday) > $now) {
      push(@result, $bm);
      $result[-1]->{hash} = $k;
      $result[-1]->{colour} = $cols{$dom};
    }
  }
  return \@result;
}

sub list_bookmarks {
  return encode_json(get_result_by_domain(shift()));
}

sub eat_page {
  my %result;
  $result{url} = shift;
  $result{url} = 'http://'.$result{url} unless ($result{url} =~ m|^https?\://|i);
  $result{title} = 'untitled : '.$result{url};

  my @us = split(/\//, $result{url});
  $us[2] =~ s/^www\.//;
  $result{domain} = $us[2];

  my $page = get($result{url});
  if (defined($page)) {
    my $tp  = HTML::TokeParser->new(\$page);
    if ($tp->get_tag("title")) {
      $result{title} = $tp->get_trimmed_text();
    }
    $result{page} = $page;
  }

  my %eaters = (
    'ebay.co.uk' => \&eat_ebay,
    'archive.org' => \&eat_archive,
  );
  if (defined($eaters{$result{domain}})) {
    $eaters{$result{domain}}->(\%result);
  }

  return \%result;
}

sub eat_archive {
}

sub eat_ebay {
  my $r = shift;

  # Turn ebay.co.uk links into just the item number.
  my @u = split(/\?/, $r->{url});
  if ($u[0] =~ m|ebay\.co\.uk/.+?(\d{12})|) {
    $r->{url} = "https://www.ebay.co.uk/itm/$1";

    # Find the auction completion date.
    my $page = $r->{page};
    my $tp  = HTML::TokeParser->new(\$page);
    while (my $i = $tp->get_tag("span")) {
      if (defined($i->[1]->{'class'})) {
        my $s = $i->[1]->{'class'};
        if ($s eq 'vi-tm-left') {
          my $t = $tp->get_phrase();
          $t =~ s/[()]//g;
          $r->{expires} = str2time($t);
        }
      }
    }
  }

  # Remove ebay title suffix.
  $r->{title} =~ s/ \| ebay$//gi;
}

sub restore_db { return (-e $dbfile) ? decode_json(read_file($dbfile)) : undef; }
sub store_db {
  my $json = JSON->new();
  write_file($dbfile, $json->pretty->canonical->encode(shift()));
}

sub get_json {
  local $/ = '}';
  my $d = <STDIN>;
  return decode_json($d);
}

sub hashed { return substr(md5_hex(shift()), 0, 12); }

sub get_index {
  return <<HTML;
<html lang="en">
<head><title>Scrap, init?</title>
<link rel="icon" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>&#x1f9f1;</text></svg>">
<script>

var lastneedle = 'eggs';
var edit = 0;
var selected = -1;
var ticker;
var interval;

window.addEventListener('keydown', function(e) {
  var handled = 0;
  var d = document.getElementById(selected);
  if (d) d.classList.remove('selected');
  if (e.keyCode == 40) {
    selected++;
    handled = 1;
  } else if (e.keyCode == 38) {
    selected = (selected < 0) ? -1 : (selected - 1);
    handled = 1;
  } else if (e.keyCode == 13) {
    d.getElementsByTagName('a')[0].click();
    handled = 1;
  }
  if (handled) {
    d = document.getElementById(selected);
    if (d) d.classList.add('selected');
    e.preventDefault();
  }
});

function setup() {
  get_marks();
}

function typed_something(fn, arg) {
  clearTimeout(ticker);
  ticker = setTimeout(function(){fn(arg);}, 1000);
}

function bactivate(n, m) {
  document.getElementById(n).style = m ? 'background-color: fuchsia' : 0;
}

function bvisible(n, m) {
  document.getElementById(n).style.visibility = m ? 'visible' : 'hidden';
}

function toggle_edit() {
  edit = 1 - edit;
  bactivate('edit', edit);
  get_marks(1);
  focusinput();
}

// mode = 0 -> get bookmarks if there's a change of needle
// mode = 1 -> get bookmarks now
function get_marks(mode) {
  var needle = document.getElementById('textinput').value;
  if ((mode == 0) && (needle == lastneedle)) {
    return;
  }
  lastneedle = needle;
  selected = -1;

  var x = new XMLHttpRequest();
  x.open('GET', '/list/'+needle);
  x.send();
  x.onload = (e) => {
    var data = JSON.parse(x.response);
    var list = '';
    var lm;
    var now = Date.now();
    data.forEach((d, i) => {
      var h = d['hash'];
      var m = d['domain'];
      var ms = '';
      if (m == lm) {
        m = '';
        ms = 'border: none';
      }
      lm = d['domain'];
      var u = d['url'];
      var t = d['title'];
      var v = d['visit'];
      v += (v == 1) ? '&nbsp;visit' : '&nbsp;visits';
      var e = d['expires'];
      var c = d['colour'];

      var cl = 'tile ';
      var st = 'background-color: '+c;
      var ed = '';

      if (e > 0) {
        var diff = e - (now / 1000);
        if (diff <= 0) {
          st += '; opacity: .1';
        } else if (diff < (24*60*60)) {
          cl += 'expiring';
          ed = (diff < (100 * 60)) ? Math.floor(diff/60) + 'm' : Math.floor(diff/(60*60)) + 'h';
          ed = '<div class="timeleft">'+ed+'</div>';
        }
        e = new Date(e * 1000).toLocaleString('en-GB', { dateStyle: 'long', timeStyle: 'short' } );
      }

      var lnk;
      if (edit == 1) {
        lnk = '<div id="'+h+'" contenteditable="true" onblur=record_edit("'+h+'")>'+t+'</div>';
      } else {
        lnk = '<a target="_blank" rel="noopener noreferrer" onmouseup=visit("'+u+'") href="'+u+'">'+t+'</a>'+ed;
      }
      list += '<div ';
      if (e) list += 'title="'+e+'" '
      list += 'class="'+cl+'" id="'+i+'" style="'+st+'">'+lnk+'</div>';
    });
    show_marks(list);

    clearInterval(interval);
    interval = setInterval(function(){get_marks(1);}, 5 * 60 * 1000);
    document.getElementById('date').innerHTML = new Date().toLocaleString('en-GB', { dateStyle: 'long', timeStyle: 'short' } );
  }
}

function show_marks(m) {
  document.getElementById('marks').innerHTML = m;
}

function record_edit(h) {
  var d = { 'hash' : h, 'title' : document.getElementById(h).innerHTML };
  poster('edit', d);
}

function del_mark(h) {
  var d = { 'hash' : h };
  poster('delete', d);
}

function visit(u) {
  var d = { 'url' : u };
  poster('visit', d);
}

function poster(ep, obj, cb) {
  var x = new XMLHttpRequest();
  x.open('POST', '/'+ep);
  x.setRequestHeader('Content-type', 'application/json');
  x.send(JSON.stringify(obj));
  x.onload = (e) => {
    var data = JSON.parse(x.response);
    if (typeof(cb) === 'undefined') {
      get_marks(1);
    } else {
      cb(data);
    }
  }
}

function focusinput() {
  if (edit == 1) return;

  setTimeout(function() {
    document.getElementById('textinput').focus();
  }, 200);
}

function handlekey(event) {
  if (event.key === "Escape") {
    document.getElementById('textinput').value = '';
    get_marks();
  }
}
</script>
<style>
body {
  color: black;
  font-family: sans-serif;
}

.header {
  position: fixed;
  width: 100%;
  z-index: 2;
}

.marks {
  padding-top: 60px;
  margin: 10px auto;
}

.tile {
  float: left;
  padding: 3px 6px;
  margin: 2px;
  text-align: left;
  width: 140px;
  height: 38px;
  font-size: 10px;
  border-radius: 8px;
  overflow: hidden;
  position: relative;
  letter-spacing: -0.2px;
}

.selected {
  border: 2px dashed fuchsia;
  padding: 1px 4px;
}

.timeleft {
  position: absolute;
  bottom: 3px;
  right: 2px;
  opacity: .3;
  font-size: 28px;
  font-weight: bold;
  color: red;
  z-index: 3;
}

.edit {
  background-color: #eee;
  margin: 0px 40px;
  padding: 7px 10px;
  cursor: pointer;
  border: 6px solid white;
  float: right;
}

.date {
  color: #ddd;
  font-size: 8px;
  vertical-align: top;
  margin-top: 4px;
}

input {
  border: 4px dotted #ddd;
  width: 55em;
  padding: 10px;
  margin: 2px 10px;
}

.domain, .visits {
  font-size: 10px;
  font-weight: lighter;
}
.domain { text-align: right; }

a {
  color: black;
  display: block;
  height: 100%;
  width: 100%;
  text-decoration: none;
}
a:visited { color: black; }
a:hover { color: fuchsia; }

.hidden { visibility: hidden; }

[contenteditable=true]:focus {
  background-color: fuchsia;
  color: white;
  font-weight: bold;
}
</style>
</head>
<body onload="setup()">
<div class="header">
<input id="textinput" onkeyup="handlekey(event)" onblur="focusinput()" autofocus oninput="typed_something(get_marks)">
<span id="edit" class="edit" style="float:right; margin-right:40px" onclick="toggle_edit()">Edit</span>
<span id="date" class="date"></span>
</div>
<div class="marks" id="marks"></div>
</body>
</html>
HTML
}
